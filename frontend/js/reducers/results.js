
import { 
  NORMALIZING,
  PREDICTING,
  RECEIVE_NORMALIZED,
  RECEIVE_SCORES,
  RESET,
} from '../constants/actionTypes';

export default (
  state = {
      isNormalizing: false,
      isPredicting: false,
      normalizedClaim: "",
      normalizedSentenceMap: {}, // sentence id -> normalized sentence
      scoreMap: {},              // sentence id -> score
  },
  action
) => {
  switch(action.type) {
    case NORMALIZING:
      return Object.assign({}, state, { isNormalizing: true });
    case PREDICTING:
      return Object.assign({}, state, { isPredicting: true });
    case RECEIVE_NORMALIZED:
      return Object.assign({}, state, {
        isNormalizing: false,
        normalizedClaim: action.normalizedClaim,
        normalizedSentenceMap: action.normalizedSentenceMap,
        scoreMap: {}
      });
    case RECEIVE_SCORES:
      return Object.assign({}, state, {
        isPredicting: false,
        scoreMap: action.scoreMap
      });
    case RESET:
      return Object.assign({}, state, {
        isNormalizing: false,
        normalizedClaim: "",
        normalizedSentenceMap: {},
        scoreMap: {}
      });
    default:
      return state;
  }
}
