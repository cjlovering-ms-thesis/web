import React from 'react'
import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';
import ResultGrid from './ResultGrid';
import { Paper } from 'material-ui';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import Space from './Space';

export default class ResultPane extends React.Component {
  static propTypes = {
    claim: PropTypes.string.isRequired,
    sentences: PropTypes.object.isRequired,
    results: PropTypes.object.isRequired,
    changeClaim: PropTypes.func.isRequired,
    predict: PropTypes.func.isRequired,
    normalize: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    tokenize: PropTypes.func.isRequired
  }
  state = { viewPaper: false }
  render() {
    const {
      claim,
      sentences,
      results,
      changeClaim,
      normalize,
      predict,
      reset,
      tokenize,
      viewPaper
    } = this.props;
    const sentencesNormalized = Object.keys(results.normalizedSentenceMap).length > 0;
    const clearButton = (
      <RaisedButton
        label="Go Back"
        onClick={() => reset()}
      />
    );
    const normalizeButton = (
      <RaisedButton
        label="Normalize Sentences"
        onClick={() => normalize(claim, sentences.sentenceMap)}
      />
    );
    const predictButton = (
      <RaisedButton
        label="Find Evidence"
        onClick={() => predict(results.normalizedClaim, results.normalizedSentenceMap)}
      />
    );
    const switchViewButton = (
      <RaisedButton
        label={this.state.viewPaper ? "View Details" : "View Paper" }
        onClick={() => this.setState({viewPaper: !this.state.viewPaper})}
      />
    )
    const content = this.state.viewPaper ? (
      <Card initiallyExpanded={true}>
        <CardHeader
          title="Document Heatmap"
          actAsExpander={false}
          showExpandableButton={false}
        />
        <CardText expandable={false}>
          {
            sentences.sentenceIdx.map(idx => results.normalizedSentenceMap[idx]).join('. ')
          }
        </CardText>
    </Card>
    ) : (
      <ResultGrid
        {...sentences}
        {...results}
      />
    )
    const claimJSX = results.normalizedClaim.length > 0 ? (
      <Card initiallyExpanded={true}>
        <CardHeader
          title="Normalized Claim"
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true}>
          {results.normalizedClaim}
        </CardText>
    </Card>
    ) : null;
    return (
      <div>
        {clearButton}
        {sentencesNormalized ? predictButton : normalizeButton}
        {switchViewButton}
        {claimJSX}
        <Space />
        {content}
      </div>
    )
  }
}
