import React from 'react'
import PropTypes from 'prop-types';

import InputPane from './InputPane';
import Progress from './Progress';
import ResultPane from './ResultPane';
import Space from './Space';
import Title from './Title';

export default class MainPane extends React.Component {
  static propTypes = {
    claim: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    sentences: PropTypes.object.isRequired,
    changeClaim: PropTypes.func.isRequired,
    changeText: PropTypes.func.isRequired,
    normalize: PropTypes.func.isRequired,
    predict: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    tokenize: PropTypes.func.isRequired
  }
  render() {
    const { 
      claim,
      results,
      sentences,
      text,
      changeClaim,
      changeText,
      normalize,
      predict,
      reset,
      tokenize,
      viewPaper
    } = this.props;
    const showingText = sentences.sentenceIdx.length == 0;
    const content = showingText ? (
      <InputPane
        claim={claim}
        text={text}
        changeClaim={changeClaim}
        changeText={changeText}
        tokenize={tokenize}
      /> 
    ) : (
      <ResultPane
        claim={claim}
        results={results}
        sentences={sentences}
        text={text}
        changeClaim={changeClaim}
        normalize={normalize}
        predict={predict}
        reset={reset}
        tokenize={tokenize}
        viewPaper={viewPaper}
      />
    )
    return (
      <div>
        <Title reset={reset}/>
        <Space />
        {content}
        <Progress
          results={results}
          sentences={sentences}
        />
      </div>
    )
  }
}
