import 'whatwg-fetch';
import { receiveNormalized } from './receive'
import { NORMALIZING, TOKENIZE_SENTENCES } from '../constants/actionTypes';

const normalizingSentences = text => {
  return {
    type: NORMALIZING
  };
};

const normalize = (claim, sentenceMap) => {
  return dispatch => {
    dispatch(normalizingSentences());
    return fetch(`/normalize/`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          'claim': claim,
          'sentences': sentenceMap
        })
    })
    .then(
        response => response.json(),
        error => console.log('An error occured when fetching text entries.', error)
    )
    .then(json => dispatch(receiveNormalized(json)));
  }
}

export { normalize };
