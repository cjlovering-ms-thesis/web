const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'js/index.jsx'),
    output: {
        path: path.join(__dirname, '../servers/demo/static/js/'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        }
      ]
    }
};
