import {
  CHANGE_TEXT
} from '../constants/actionTypes';

export default (
  state = "",
  action
) => {
  switch(action.type) {
      case CHANGE_TEXT:
        return action.text;
      default:
        return state;
  }
}
