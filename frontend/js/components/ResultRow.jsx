import React from 'react';
import PropTypes from 'prop-types';

import {
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';

export default class ResultRow extends React.Component {
  static propTypes = {
    idx: PropTypes.string.isRequired,
    normalizedSentence: PropTypes.string.isRequired,
    rank: PropTypes.number.isRequired,
    sentence: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
  }
  render() {
    const { idx, normalizedSentence, rank, sentence, score } = this.props;
    return (
      <TableRow>
          <TableRowColumn width={"35px"}>
              {idx}
          </TableRowColumn>
          <TableRowColumn  width={"35px"}>
              {rank}
          </TableRowColumn>
          <TableRowColumn  width={"35px"}>
              {score.toFixed(2)}
          </TableRowColumn>
          <TableRowColumn>
              {normalizedSentence.length > 0 ? normalizedSentence : sentence}
          </TableRowColumn>
      </TableRow>
    );
  }
}