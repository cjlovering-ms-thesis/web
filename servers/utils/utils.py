import torch
import claimai

from typing import Dict, List

from nltk.tokenize import sent_tokenize

def get_config():
  """Configuration for the model.
  """
  return {
      'bidirectional': True,
      'hidden_size': 80,
      'num_enc_layers': 1,
      'encoder': torch.nn.LSTM,
  }

def load_model(model: torch.nn.Module=claimai.BiDafR, state: str="00001") -> torch.nn.Module:
  """Use claimai API to load a trained model into memory.

  Args:
    model (torch.nn.Module): model type.
    state (str): the version (file name) of the model.
  Returns:
    model: trained instance of the model.
  """
  state_path = 'bins/state/{}.pt'.format(state)
  return claimai.load(state_path, model, get_config())

def predict(claim: str, candidates: Dict[int, str], model: torch.nn.Module) -> Dict[int, int]:
  """Predicts (regresses) evidence scores for each candidate for the claim.

  Args:
    claim (str): the claim made.
    candidates (Dict[int, str]): the dict of candidate sentences (idx -> sen).
    model (torch.nn.Module): a trained model instance
  Returns:
    A mapping between sentence idx and score.
  """
  data = [(claim, candidate) for candidate in candidates.values()]
  C,E = zip(*data)
  X = C,E
  results = claimai.predict(X, model)
  # results are in the form of [[score1], ...]
  flatten = [s[0] for s in results]
  return dict(zip(candidates.keys(), flatten))

def normalize(strings: Dict[int, str]) -> Dict[int, str]:
  """Normalize text before using model. Splits text.

  Args:
    strings (Dict[int, str]): strings to be normalized.
  Returns:
    normalized version of the sentences.
  """
  idx_map = [idx for idx in strings.keys()]
  strings = [s for s in strings.values()]
  normalized = claimai.normalize(strings)
  return {idx_map[i]:n_s for i,n_s in enumerate(normalized)}

def tokenize(text: str) -> Dict[int, str]:
  """Tokenize text. 
  
  Args:
    text (str): A single string to be split into sentences.
  Returns:
    Dict[int, str]: A map of index (location in document) to sentence contenxts.
  """
  return {i:sent for i,sent in enumerate(sent_tokenize(text))}
