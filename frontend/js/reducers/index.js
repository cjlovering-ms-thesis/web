import { combineReducers } from 'redux';
import claim from './claim';
import text from './text';
import sentences from './sentences';
import results from './results';

const root = combineReducers({
  claim,
  results,
  sentences,
  text
})

export default root;