import { changeText } from './text';
import { changeClaim } from './claim';
import { predict } from './predict';
import { normalize } from './normalize';
import { receiveTokenizedText } from './receive';
import { reset } from './reset';
import { tokenize } from './tokenize';

export {
  changeClaim,
  changeText,
  predict,
  normalize,
  receiveTokenizedText,
  reset,
  tokenize,
};
