import 'whatwg-fetch';
import { receiveScores } from './receive'
import { PREDICTING } from '../constants/actionTypes';

const predicting = text => {
  return {
    type: PREDICTING
  };
};

const predict = (claim, normalizedSentences) => {
  return dispatch => {
    dispatch(predicting());
    return fetch(`/predict/`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          'claim': claim,
          'sentences': normalizedSentences
        })
    })
    .then(
        response => response.json(),
        error => console.log('An error occured when fetching text entries.', error)
    )
    .then(json => dispatch(receiveScores(json)));
  }
}

export { predict };