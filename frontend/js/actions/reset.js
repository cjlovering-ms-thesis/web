import { 
  RESET,
} from '../constants/actionTypes';

const reset = () => {
  return {
      type: RESET,
  };
};

export { reset };
