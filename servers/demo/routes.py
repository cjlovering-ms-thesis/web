from flask import jsonify, render_template, request

from demo import app
from utils import load_model, predict, normalize, tokenize

# server start up
model = load_model()

@app.route("/")
def index():
  """Renders template fetches js """
  return render_template('index.html')

@app.route("/tokenize/", methods=['POST'])
def tokenize_route():
  """Tokenize text into sentences.
  """
  text = request.get_json()['text']
  tokens = {
    'sentences': tokenize(text)
  }
  return jsonify(tokens)

@app.route("/normalize/", methods=['POST'])
def normalize_route():
  """Tokenize text into sentences.
  """
  data = request.get_json()
  claim = data['claim']
  sentences = data['sentences']
  normalized_sentences = normalize(sentences)
  normalized_claim = normalize({0: claim})[0]
  normalized = {
    'sentences': normalized_sentences,
    'claim': normalized_claim
  }
  return jsonify(normalized)

@app.route("/predict/", methods=['POST'])
def predict_route():
  """Process tuple of sentence and claims.
  """
  data = request.get_json()
  claim = data['claim']
  candidates = data['sentences']
  scores = predict(claim, candidates, model)
  score_map = {
    'scores': scores
  }
  return jsonify(score_map)

@app.after_request
def add_header(r):
  """Modifies request header
  """
  r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
  r.headers["Pragma"] = "no-cache"
  r.headers["Expires"] = "0"
  r.headers['Cache-Control'] = 'public, max-age=0'
  return r