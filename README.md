# web

## Contents

### fe

Frontend interfaces and logic.

### servers

Contains demo server and utils.

## Instructions

```bash
cd frontend
npm run prod     # Read package.json to choose build script.
cd ..
cd servers
python3 run.py
```