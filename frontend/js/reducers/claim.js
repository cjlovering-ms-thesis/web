import {
  CHANGE_CLAIM
} from '../constants/actionTypes';

export default (
  state = "",
  action
) => {
  switch(action.type) {
      case CHANGE_CLAIM:
        return action.claim;
      default:
        return state;
  }
}
