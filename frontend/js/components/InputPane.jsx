import React from 'react';
import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

export default class InputPane extends React.Component {
  static propTypes = {
    claim: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    changeClaim: PropTypes.func.isRequired,
    changeText: PropTypes.func.isRequired,
    tokenize: PropTypes.func.isRequired
  }
  render() {
    const {
      claim,
      text,
      changeClaim,
      changeText,
      tokenize
    } = this.props;
    return (
      <div>
        <div>
          <TextField
            name="claim sentence"
            fullWidth={true}
            multiLine={true}
            hintText={"Enter a claim from a referenced academic paper."}
            value={claim}
            onChange={e => changeClaim(e.target.value)}
          />
        </div>
        <div>
          <TextField
            name="referenced document"
            fullWidth={true}
            multiLine={true}
            hintText={"Enter a text version of an academic paper."}
            value={text}
            onChange={e => changeText(e.target.value)}
          />
        </div>
        <RaisedButton label="Tokenize Text" onClick={() => tokenize(text)} />
        <RaisedButton label="Clear All" onClick={() => this.clear()} />
      </div>
    );
  }
  clear() {
    const {
      changeClaim,
      changeText
    } = this.props;
    changeClaim("");
    changeText("");
  }
}

