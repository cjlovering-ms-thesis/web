import { 
  CHANGE_TEXT,
} from '../constants/actionTypes';

const changeText = text => {
  return {
      type: CHANGE_TEXT,
      text: text
  };
};

export { changeText };
