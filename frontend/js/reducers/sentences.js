
import { 
  TOKENIZING,
  RECEIVE_TOKENIZED_TEXT,
  RESET,
} from '../constants/actionTypes';

/* sentences */
export default (
  state = {
      isTokenizing: false,  // is fetching text result
      sentenceIdx: [],      // a list of text entry sentence ids 
      sentenceMap: {},      // map of sentence id to sentence
  },
  action
) => {
  switch(action.type) {
    case RESET:
      return {
        isTokenizing: false,
        sentenceIdx: [],
        sentenceMap: {}
      }
    case TOKENIZING:
      return {
        isTokenizing: true,
        sentenceIdx: [],
        sentenceMap: {}
      }
    case RECEIVE_TOKENIZED_TEXT:
      return {
        isTokenizing: false,
        sentenceIdx: action.sentenceIdx,
        sentenceMap: action.sentenceMap
      }
    default:
      return state;
  }
}
