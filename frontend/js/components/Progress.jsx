import React from 'react';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';

class Progress extends React.Component {

  getStep() {
    const {results, sentences} = this.props;
    if (Object.keys(results.scoreMap).length > 0) {
      return 3;
    }
    if (results.normalizedClaim.length > 0) {
      return 2;
    }
    if (sentences.sentenceIdx.length > 0) {
      return 1;
    }
    return 0;
  }

  render() {
    const stepIndex = this.getStep()
    const contentStyle = {margin: '0 16px'};

    return (
      <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Enter text.</StepLabel>
          </Step>
          <Step>
            <StepLabel>Tokenize text.</StepLabel>
          </Step>
          <Step>
            <StepLabel>Normalize text.</StepLabel>
          </Step>
          <Step>
            <StepLabel>Find evidence!</StepLabel>
          </Step>
        </Stepper>
      </div>
    );
  }
}

export default Progress;