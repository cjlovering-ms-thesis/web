import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import ResultRow from './ResultRow';

import { ORDER_BY_RANK, ORDER_BY_INDEX } from '../constants/orderTypes'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

export default class ResultGrid extends React.Component {
  static propTypes = {
    sentenceIdx: PropTypes.array.isRequired,
    sentenceMap: PropTypes.object.isRequired,
    scoreMap: PropTypes.object.isRequired,
    normalizedClaim: PropTypes.string.isRequired,
    normalizedSentenceMap: PropTypes.object.isRequired
  }
  state = {
    orderBy: ORDER_BY_INDEX
  }
  render() {
    const {
      sentenceIdx,
      sentenceMap,
      scoreMap,
      normalizedClaim,
      normalizedSentenceMap,
      isNormalizing,
      isPredicting
    } = this.props;
    const hasNormalized = !isNormalizing && (Object.keys(normalizedSentenceMap).length > 0);
    const hasPredicted = !isPredicting && (Object.keys(scoreMap).length > 0);
    const rankMap = hasPredicted ? (
      sentenceIdx
        .map((idx, i) => [scoreMap[idx], idx]) // map to score, idx, i
        .sort(([score1], [score2]) => score2 - score1) // sort on idx
        .map(([, idx]) => idx) // strip the score
        .map((idx, argIdx) => [idx, argIdx]) // add the location in list
        .reduce((map, obj) => { // create map, idx -> argIdx
          map[obj[0]] = obj[1];
          return map;
        }, {})
    ) : {};
    let order;
    switch(this.state.orderBy) {
      case ORDER_BY_INDEX:
        order = sentenceIdx;
        break;
      case ORDER_BY_RANK:
        order = sentenceIdx.map(idx => [rankMap[idx], idx])
            .sort(([rank1], [rank2]) => rank1 - rank2)
            .map(([,idx]) => idx)
        break;
      default:
        order = sentenceIdx;
    }
    const display = (
      <div>
        <div>
        <Table>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false} enableSelectAll={true}>
            <TableRow>
              <TableHeaderColumn 
                width={"35px"} 
              > 
                <div 
                  onClick={() => this.setState({orderBy: ORDER_BY_INDEX})}
                > 
                  Index
                </div>
              </TableHeaderColumn>
              <TableHeaderColumn width={"35px"}>
                <div
                  onClick={() => this.setState({orderBy: ORDER_BY_RANK})}
                >
                  Rank
                </div>
              </TableHeaderColumn>
              <TableHeaderColumn width={"35px"}>Score</TableHeaderColumn>
              <TableHeaderColumn>Sentence</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} stripedRows={true}>
            {
              order.map(idx => this.generateRow(idx, hasNormalized ? normalizedSentenceMap[idx] : "", hasPredicted ? rankMap[idx] : 0, hasPredicted ? scoreMap[idx] : -1, sentenceMap[idx]))
            }
          </TableBody>
        </Table>
        </div>
      </div>
    );
    return (
      <div>
          {display}
      </div>
    );
  }
  generateRow(idx, normalizedSentence, rank, score, sentence) {
    return (
      <TableRow key={shortid.generate()}>
          <TableRowColumn width={"35px"}>
              {idx}
          </TableRowColumn>
          <TableRowColumn  width={"35px"}>
              {rank}
          </TableRowColumn>
          <TableRowColumn  width={"35px"}>
              {score.toFixed(2)}
          </TableRowColumn>
          <TableRowColumn style={{ whiteSpace: 'normal', wordWrap: 'break-word' }}>
              {normalizedSentence.length > 0 ? normalizedSentence : sentence}
          </TableRowColumn>
      </TableRow>
    );
  }
}