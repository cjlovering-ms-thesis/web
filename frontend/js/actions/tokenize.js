import 'whatwg-fetch';
import { receiveTokenizedText } from './receive'
import { TOKENIZING, TOKENIZE_SENTENCES } from '../constants/actionTypes';

const tokenizingText = text => {
  return {
    type: TOKENIZING,
    text
  };
};

const tokenize = text => {
  return dispatch => {
    
    // Notify App that async call is being made.
    dispatch(tokenizingText(text));

    return fetch(`/tokenize/`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          'text': text
        })
    })
    .then(
        response => response.json(),
        error => console.log('An error occured when fetching text entries.', error)
    )
    .then(json => dispatch(receiveTokenizedText(json)))
  }
}

export { tokenize };