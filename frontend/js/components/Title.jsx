import React from 'react';
import AppBar from 'material-ui/AppBar';

export default class Title extends React.Component {
  render() {
    const {
      reset
    } = this.props;

    return (
      <AppBar title={"ClaimAI"} onTitleClick={() => reset()} />
    );
  }
}