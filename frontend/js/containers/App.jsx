import React from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import MainPane from '../components/MainPane'
import * as Actions from '../actions'

const App = ({
  claim,
  results,
  sentences,
  text,
  actions
}) => (
  <MainPane
    claim={claim}
    results={results}
    sentences={sentences}
    text={text}
    changeClaim={actions.changeClaim}
    changeText={actions.changeText}
    normalize={actions.normalize}
    predict={actions.predict}
    reset={actions.reset}
    tokenize={actions.tokenize}
    viewPaper={actions.viewPaper}
  />
);

App.propTypes = {
  claim: PropTypes.string.isRequired,
  results: PropTypes.object.isRequired,
  sentences: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  claim: state.claim,
  results: state.results,
  sentences: state.sentences,
  text: state.text
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App)