import { 
  RECEIVE_SCORES,
  RECEIVE_TOKENIZED_TEXT,
  RECEIVE_NORMALIZED,
} from '../constants/actionTypes';

const receiveNormalized = json => {
  return {
    type: RECEIVE_NORMALIZED,
    normalizedSentenceMap: json.sentences,
    normalizedClaim: json.claim
  };
};

const receiveScores = json => {
  return {
    type: RECEIVE_SCORES,
    scoreMap: json.scores
  };
};

const receiveTokenizedText = json => {
  return {
    type: RECEIVE_TOKENIZED_TEXT,
    sentenceIdx: Object.keys(json.sentences),
    sentenceMap: json.sentences
  };
};

export {
  receiveNormalized,
  receiveScores,
  receiveTokenizedText
};
