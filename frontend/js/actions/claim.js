import { 
  CHANGE_CLAIM,
} from '../constants/actionTypes';

const changeClaim = claim => {
  return {
      type: CHANGE_CLAIM,
      claim: claim
  };
};

export { changeClaim };
